An entity–relationship model (ER model for short) describes interrelated things of interest in a specific domain of knowledge. A basic ER model is composed of entity types (which classify the things of interest) and specifies relationships that can exist between instances of those entity types. 

ER model becomes an abstract data model, that defines a data or information structure which can be implemented in a database, typically a relational database. It is usually drawn in a graphical form as boxes (entities) that are connected by lines (relationships) which express the associations and dependencies between entities.

Entity–relationship modeling was `developed` for database design by `Peter Chen `

There is a tradition for ER/data models to be built at two or three levels of abstraction.

Conceptual model - Semantics 
----------------
This is the highest level ER model in that it contains the least granular detail but establishes the overall scope of what is to be included within the model set. The conceptual ER model normally defines master reference data entities that are commonly used by the organization. Developing an enterprise-wide conceptual ER model is useful to support documenting the data architecture for an organization.

A conceptual ER model may be used as the foundation for one or more logical data models 

- Includes high-level data constructs 
- `Non-technical names`, so that executives and managers at all levels can understand the data basis of Architectural Description
- Uses general high-level data constructs from which Architectural Descriptions are created in non-technical terms
- May not be normalized 
- Represented in the DIV-1 Viewpoint (DoDAF V2.0) 


Logical Data model - Technology
------------------

A logical data model or logical schema is a data model of a specific problem domain expressed independently of a particular database management product or storage technology (physical data model) but in terms of `data structures such as relational tables and columns`, `object-oriented classes`, or `XML tags`. This is as opposed to a conceptual data model, which describes the semantics of an organization without reference to technology.

#### Reasons for building a logical data structure

- Helps common understanding of business data elements and requirements
- Provides foundation for designing a database
- Facilitates avoidance of data redundancy and thus prevent data & business transaction inconsistency
- Facilitates data re-use and sharing
- Decreases development and maintenance time and cost
- Confirms a logical process model and helps impact analysis.

Logical Data model Features
---------------------------

- Includes entities (tables), attributes (columns/fields) and relationships (keys) 

- Uses business names for entities & attributes

- Is independent of technology (platform, DBMS) 

- Is normalized to fourth normal form (4NF) 

- Represented in the DIV-2 Viewpoint (DoDAF V2.0), and OV-7 View (DoDAF V1.5)



Physical Data model 
-------------------




