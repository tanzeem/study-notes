

Shopping Cart Application
===============


 Database Model
--------


### Overview

 
#### Diagrams

 -   **Admin**
      -  Change Sets
      -  Modules
      -  Security
      
 -   **Catalog**
       - Category
       - Product
       - Reviews
       - Search
       - Sku*
       
 -   **CMS**
       - Field
       - Page
       - Static Asset
       - Structured Content
       
 -   **Customer**
       - Detail
       
 -   **Offer**
       - Detail
       
 -   **Order**
       - Detail
       - Fulfillment
       - Payment
       - Tax
       
 -   **Other**
       - Common
       - Email Tracking
       - Fulfillment
       - Social

*SKU (Stock Keeping Unit)

### Admin Model

#### Detailed Diagrams

-    Change Sets
-    Modules
-    Security

**High Level Diagram**

![Admin-HighLevel-ERD](./shopping-cart-application/AdminHighLevelERD.png  "Admin-HighLevel-ERD")

**Admin Change Sets**

![Admin Change Sets Detailed ERD](./shopping-cart-application/AdminChangeSetsDetailedERD.png  "Admin Change Sets Detailed ERD")

**Detailed ERD**

**Tables**

	Table 					Related Entity 		Description
	
	BLC_SANDBOX 			SandBox 			Represents a sandbox.
	BLC_ADMIN_USER_SANDBOX 	- 				Cross reference table that points to an admin user.
	BLC_SITE_SANDBOX 			- 				Cross reference table that points to a site.
	BLC_SANDBOX_ITEM 		SandBoxItem 			Represents a sandbox item.
	SANDBOX_ITEM_ACTION 	- 					Cross reference table that points to an action.
	BLC_SANDBOX_ACTION 	SandBoxAction 		Represents a sandbox action.
	
**Related Tables**

	Table 			Related Entity 		Description
	
	BLC_ADMIN_USER 	AdminUser 			Represents an admin user.
	BLC_SITE 		Site 				Represents a site

**Admin Security**

***

**Detailed ERD**
![Admin Security Detailed ERD](./shopping-cart-application/AdminSecurityDetailedERD.png  "Admin Security Detailed ERD")

**Tables**


	Table 						Related Entity 			Description

	BLC_ADMIN_USER 				AdminUser 			Represents an admin user.
	BLC_ADMIN_USER_ROLE_XREF 		- 			Cross reference table that points to an admin user role.
	BLC_ADMIN_ROLE 				AdminRole 			Represents an admin user role.
	BLC_ADMIN_USER_PERMISSION_XREF 	- 			Cross reference table that points to an admin user permission.
	BLC_ADMIN_PERMISSION 		AdminPermission 	Represents an admin user permission.
	BLC_ADMIN_ROLE_PERMISSION_XREF 	- 				Cross reference table that points to an admin role permission.
	BLC_ADMIN_PERMISSION_ENTITY 	AdminPermissionQualifiedEntity 	Represents an admin user permission entity.

**Related Tables**

	Table 			Related Entity 		Description
	
	BLC_SANDBOX 	SandBox 			Represents a sandbox.
