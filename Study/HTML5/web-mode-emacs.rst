web-mode.el
===========

About
-----

`web-mode.el` is an autonomous **emacs** major-mode for editing web templates.
HTML documents can embed parts (CSS / JavaScript) and blocks (client / server side). 

See web-mode.org


Download
--------

You can download this mode [here](https://raw.githubusercontent.com/fxbois/web-mode/master/web-mode.el)

A GitHub repository is available: http://github.com/fxbois/web-mode/

web-mode.el is also available on melpa and on melpa stable.

web-mode.el is GPL and Free Software.

Install
-------

First drop the file `web-mode.el` in a directory defined in your `load-path`.
Then, add in your .emacs
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))


Default value of `load-path'
----------------------------

On GNU/Linux, the default value of ‘load-path’ includes two special directories and their descendants:
`/usr/local/share/emacs/VERSION/site-lisp` and `/usr/local/share/emacs/site-lisp`.
The first directory contains packages for a particular Emacs version; the second contains packages for all installed versions of Emacs.
These directories contain files for the current site, for use by the system administrator when installing software locally[1].

`~/.emacs.d/`, on the other hand, contains `files for the current user`, and is independent of system-wide changes.
This makes it the best choice for storing your personal changes. Installing all packages in a sub-directory of `~/.emacs.d/` also makes it very easy to move them along with your configuration to a different machine.
