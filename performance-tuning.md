# Section 1 

--------------------------
Using the Top Command
--------------------------

comes with the “procps-ng” package.

The top command allows users to monitor processes and system resource usage on Linux. It is one of the most useful tools in a sysadmin’s toolbox, and it comes pre-installed on every distribution. Unlike other commands such as ps, it is interactive, and you can browse through the list of processes, kill a process, and so on.

The top command is extremely helpful for monitoring and managing processes on a Linux system. 

Line 1

   - The time
   - How long the computer has been running
   - Number of users
   - Load average (The load average shows the system load time for the last 1, 5 and 15 minutes)

Line 2

   - Total number of tasks
   - Number of running tasks
   - Number of sleeping tasks
   - Number of stopped tasks
   - Number of zombie tasks

Line 3

   - CPU usage as a percentage by the user
   - CPU usage as a percentage by system
   - CPU usage as a percentage by low priority processes
   - CPU usage as a percentage by idle processes
   - CPU usage as a percentage by io wait
   - CPU usage as a percentage by hardware interrupts
   - CPU usage as a percentage by software interrupts
   - CPU usage as a percentage by steal time

Line 3

   - Total system memory
   - Free memory
   - Memory used
   - Buffer cache

Line 4

   - Total swap available
   - Total swap free
   - Total swap used
   - Available memory

Understanding top’s interface
=============================

	$top

The `upper half` of the output `contains statistics on processes and resource usage`, 
while the `lower half` contains `a list of the currently running processes`. 
You can use the `arrow keys` and `Page Up/Down` keys to browse through the list. If you want to quit, simply press “q”.

The Summary area
----------------
![summary-area](./top-command/top-uptime-users.png  "Summary Area")

- **System time, uptime and user sessions**

At the very top left of the screen (as marked in the screenshot above), top displays the current time. This is followed by the system uptime, which tells us the time for which the system has been running. For instance, in our example, the current time is “15:39:37”, and the system has been running for 90 days, 15 hours and 26 minutes.

Next comes the number of active user sessions. In this example, there are two active user sessions. These sessions may be either made on a TTY (physically on the system, either through the command line or a desktop environment) or a PTY (such as a terminal emulator window or over SSH). In fact, if you log in to a Linux system through a desktop environment, and then start a terminal emulator, you will find there will be two active sessions.

If you want to get more details about the active user sessions, use the who command.

- **Memory usage**

The “memory” section shows information regarding the memory usage of the system. The lines marked “Mem” and “Swap” show information about RAM and swap space respectively. Simply put, a swap space is a part of the hard disk that is used like RAM. When the RAM usage gets nearly full, infrequently used regions of the RAM are written into the swap space, ready to be retrieved later when needed. However, because accessing disks are slow, relying too much on swapping can harm system performance.

As you would naturally expect, the “total”, “free” and “used” values have their usual meanings. The “avail mem” value is the amount of memory that can be allocated to processes without causing more swapping.

The Linux kernel also tries to reduce disk access times in various ways. It maintains a “disk cache” in RAM, where frequently used regions of the disk are stored. In addition, disk writes are stored to a “disk buffer”, and the kernel eventually writes them out to the disk. The total memory consumed by them is the “buff/cache” value. It might sound like a bad thing, but it really isn’t — memory used by the cache will be allocated to processes if needed.

- **Tasks**

The “Tasks” section shows statistics regarding the processes running on your system. The “total” value is simply the total number of processes. For example, in the above screenshot, there are 27 processes running. To understand the rest of the values, we need a little bit of background on how the Linux kernel handles processes.

Processes perform a mix of I/O-bound work (such as reading disks) and CPU-bound work (such as performing arithmetic operations). The CPU is idle when a process performs I/O, so OSes switch to executing other processes during this time. In addition, the OS allows a given process to execute for a very small amount of time, and then it switches over to another process. This is how OSes appear as if they were “multitasking”. Doing all this requires us to keep track of the “state” of a process. In Linux, a process may be in of these states

1.   **Runnable (R)**: A process in this state is either executing on the CPU, or it is present on the run queue, ready to be executed.
2.   **Interruptible sleep (D)**: Processes in this state are waiting for an event to complete.
3.    **Uninterruptible sleep (S)**: In this case, a process is waiting for an I/O operation to complete.
4.   **Stopped (T):** These processes have been stopped by a job control signal (such as by pressing Ctrl+Z) or because they are being traced.
5.    **Zombie (Z)**: The kernel maintains various data structures in memory to keep track of processes. A process may create a number of child processes, and they may exit while the parent is still around. However, these data structures must be kept around until the parent obtains the status of the child processes. Such terminated processes whose data structures are still around are called zombies.

`Processes` in the `D` and `S` `states` are shown in `“sleeping”`, 
and those in the `T state are shown in “stopped”`. 

`The number of zombies are shown as the “zombie” value.`

- **CPU usage**

The CPU usage section shows the `percentage of CPU time spent` on various tasks. 

The `us` value is the `time the CPU spends`  `executing processes` in `userspace`. 
Similarly, the `sy` value is the `time spent on running kernelspace processes`.

Linux uses a “nice” value to determine the priority of a process. A `process with a high “nice” value` is “nicer” to other processes, and gets a `low priority`. Similarly, processes with a `lower “nice” gets higher priority`. As we shall see later, the default “nice” value can be changed. The time spent on executing processes with a manually set “nice” appear as the `ni` value.

This is followed by `id`, which is the `time the CPU remains idle`. Most operating systems put the CPU on a `power saving mode` when it is idle. 
Next comes the `wa` value, which is the `time the CPU spends waiting for I/O to complete`.

Interrupts are signals to the processor about an event that requires immediate attention. Hardware interrupts are typically used by peripherals to tell the system about events, such as a keypress on a keyboard. On the other hand, software interrupts are generated due to specific instructions executed on the processor. In either case, the OS handles them, and `the time spent on handling` `hardware` and `software interrupts` are given by `hi` and `si` respectively.

`In a virtualized environment, a part of the CPU resources are given to each virtual machine (VM)`. The OS detects when it has work to do, but it cannot perform them because the CPU is busy on some other VM. `The amount of time lost in this way` is the `“steal” time`, shown as `st`.

- **Load average**

The load average section represents the `average “load” over one, five and fifteen minutes`. `“Load” is a measure of the amount of computational work a system performs`. 

`On Linux`, `the load` `is` the `number of processes in` the `R` and `S states` at `any given moment`. 

The `“load average”` value gives you a `relative measure of how long you must wait for things to get done`.

Let us consider a few examples to understand this concept. 

On a `single core system`, a `load average of 0.4` means the `system is doing only 40% of work it can do`.
 A `load average of 1` means that the `system is exactly at capacity` — the `system will be overloaded by adding even a little bit of additional work`.
  A system with a load average of `2.12` means that it is `overloaded by 112% more work than it can handle`.

On a `multi-core system`, you should first `divide the load average` with the `number of CPU cores` to `get` a similar `measure`.

In addition, `“load average`” isn’t actually the typical “average” most of us know. It `is` an `“exponential moving average“`, which means a small part of the previous load averages are factored into the current value. 


The Task Area
------------------

- PID

This is the `process ID`, an `unique positive integer` that `identifies a process`.

- USER

This is the `“effective” username` (which maps to an user ID) of the `user who started the process`. Linux assigns a real user ID and an effective user ID to processes; the latter allows a process to act on behalf of another user. (For example, a non-root user can elevate to root in order to install a package.)

- PR and NI

The `“NI”` field shows the `“nice” value` of a process. The `“PR”` field shows the `scheduling priority of the process` from the `perspective of the kernel`. The `nice value affects` the `priority of a process`.

- VIRT, RES, SHR and %MEM

These three fields are `related with to memory consumption of the processes`. 
`“VIRT”` is the `total amount of memory consumed by a process`. This includes the `program’s code`, the `data stored by the process in memory`, as well as `any regions of memory` that have been `swapped to the disk`.

 `“RES”` is the `memory consumed by the process in RAM`, and `“%MEM”` expresses this value as a `percentage of the total RAM available`. Finally, `“SHR”` is the `amount of memory shared` with other processes.

-  S

As we have seen before, a process may be in various states. This field shows the `process state` in the `single-letter` form.

- TIME+

This is the `total CPU time used by the process since it started`, `precise to` the `hundredths of a second`.

- COMMAND

The `COMMAND column` shows the `name of the processes`.

Killing processes
---------------------

If you want to kill a process, simply press ‘k’ when top is running. This will bring up a prompt, which will ask for the process ID of the process and press enter.

Next, enter the signal using which the process should be killed. If you leave this blank, top uses a `SIGTERM`, which allows processes to `terminate gracefully`. If you want `to kill `a process `forcefully`, you can type in `SIGKILL` here. You can also type in the `signal number` here. For example, the number for `SIGTERM is 15` and `SIGKILL is 9.`

If you leave the process ID blank and hit enter directly, it will terminate the topmost process in the list. You can scroll using the arrow keys, and change the process you want to kill in this way.

Sorting the process list
------------------------------

One of the most frequent reasons to use a tool like top is to find out which process is consuming the most resources. 
You can press the following keys to `sort` the list:

  -  `‘M’` to `sort by memory usage`
  -  `‘P’` to sort by `CPU usage`
  -  `‘N’` to sort by `process ID`
  -  `‘T’` to sort by the `running time`
  
By `defaul`t, top displays all results in `descending order`. However, you can switch to `ascending order` by pressing `‘R’`.

sort the list with the -o switch. For example, if you want to sort processes by CPU usage, you can do so with:

	top -o %CPU

Showing a list of threads instead of processes
-----------------------------------------------------------

Processes do not share memory or other resources, making such switches rather slow. Linux, like many other operating systems, supports a “lightweight” alternative, called a “thread”. They are part of a process and share certain regions of memory and other resources, but they can't be run concurrently like processes.

By default, top shows a list of processes in its output. If you want to list the threads instead, `press` `‘H’` when top is running. Notice that the “Tasks” line says “Threads” instead, and shows the number of threads instead of processes.

It can be seen that threads have all the attributes that processes have. This is because the Linux kernel uses the same tpe of datastructures as processes.

If you want `to switch back to the process view`, `press ‘H’ again`. In addition, you can use the -H switch to display threads by default.

	top -H
	
Showing full paths
------------------------

By default, top does not show the `full path to the program`, or make a `distinction between kernelspace processes and userspace processes`. If you need this information, press` ‘c’` while top is running. Press ‘c’ again to go back to the default.

`Kernelspace processes` are marked with `square brackets` around them. As an example, in the above screenshot there are two kernel processes, kthreadd and khelper. On most Linux installations, there will usually be a few more of them.

Alternatively, you can also start top with the -c argument:

	top -c

Forest view
---------------

Sometimes, you may want `to see` the `child-parent hierarchy of processes`. 
You can see this with the forest view, by pressing `‘v’` / `’V’` while top is running.

Listing processes from an user
----------------------------------------

To list processes from a certain user, press `‘u’` when top is running. Then, `type in the username`, or leave it `blank` to display processes `for all users`.

Alternatively, you can run the top command with the -u switch. In this example, we have listed all processes from the root user.

	top -u root

Filtering through processes
-----------------------------------

`If you have a lot of processes` to work with, a simple sort won’t work well enough. In such a situation, you can use top’s `filtering` to focus on a `few processes`. To activate this mode, `press ‘o’/’O’.` A prompt appears inside top, and you can `type a filter expression` here.

A filter expression is a statement that specifies a relation between an attribute and a value. Some examples of filters are:

    COMMAND=getty: Filter processes which contain “getty” in the COMMAND attribute.
    !COMMAND=getty: Filter processes which do not have “getty” in the COMMAND attribute.
    %CPU>3.0: Filter processes which have a CPU utilization of more than 3%.

Once you’ve added a filter, you can `further prune` down things by `adding more filters`. 
To `clear` any `filters` you have added, `press ‘=’`.

Changing the default look of the CPU and memory statistics
------------------------------------------------------------------------------

You can press `‘t’ and ‘m’ ` to change the style of the CPU and memory statistics. Here is a screenshot of top, where we have pressed ‘t’ and ‘m’ once.

If you press `‘t’ or ‘m’ repeatedly`, it `cycles through four different views`. In the first two presses, it cycles through two different types of progress bars. If you press the key for a third time, the progress bar is hidden. If you press the key again, it brings back the default, text based counters.

# Section 2

Performance Tuning
============

Performance tuning is the improvement of system performance. 
The motivation for such activity is called a performance problem, which can be real or anticipated. 
Most systems will respond to increased load with some degree of decreasing performance. 
A system's ability to accept a higher load is called scalability, and modifying a system to handle a higher load is synonymous to performance tuning.

Systematic tuning follows these steps:

  *  Assess the problem and establish numeric values that categorize acceptable behavior.
  * Measure the performance of the system before modification.
  *  Identify the part of the system that is critical for improving the performance. This is called the bottleneck.
  *  Modify that part of the system to remove the bottleneck.
  * Measure the performance of the system after modification.
  * If the modification makes the performance better, adopt it. If the modification makes the performance worse, put it back to the way it was.
  
**Performance Equation**

The total amount of time (t) required to execute a particular benchmark program is



Database Tuning
===============

Database tuning describes a `group of activities` used `to optimize and homogenize the performance` of a database. 

It usually overlaps with `query tuning`, 
but refers to `design` of the `database` files, 
`selection` of the `database management system (DBMS) application`, 
and `configuration` of the `database's environment` (`operating system`, `CPU`, etc.).

Database tuning `aims` to `maximize use of system resources` to `perform work` as `efficiently` and `rapidly` as possible.


I/O tuning
----------

`Hardware` and `software` `configuration` of `disk subsystems` are `examined`: `RAID levels and configuration`, `block and stripe size allocation`, and the `configuration of` `disks`, `controller cards`, `storage cabinets`, and `external storage systems such as SANs`. 

`Transaction logs` and `temporary spaces` are `heavy consumers of I/O`, and `affect performance` for all users of the `database`. 
`Placing` them `appropriately` `is crucial`.

`Frequently joined tables` and `indexes` are `placed` so that `as they are requested from file storage`, they `can be retrieved in parallel from separate disks simultaneously`. 

`Frequently accessed tables` and `indexes` are `placed on separate disks` `to balance I/O` and `prevent read queuing`.


DBMS tuning
-----------

**DBMS users and DBA experts**

DBMS tuning refers to `tuning of the DBMS ` `and` the `configuration of the memory and processing resources` of the computer running the DBMS. This is typically done through configuring the DBMS, but the resources involved are shared with the host system.

- Tuning the DBMS can involve `setting the recovery interval` (time needed to restore the state of data to a particular point in time), `assigning parallelism` (the `breaking up of work` from a single query into tasks assigned to different processing resources), and network protocols used to communicate with database consumers.

- `Memory` is `allocated for data`, `execution plans`, `procedure cache`, and `work space`. It is much `faster to access data in memory` than data on storage, `so maintaining a sizable cache` of data `makes activities perform faster`. The same consideration is given to work space. 

- `Caching execution plans and procedures` means that they are reused instead of recompiled when needed. 

- It is important to `take as much memory as possible`, while `leaving enough for other processes and the OS` to use without excessive paging of memory to storage.

- `Processing resources` are sometimes `assigned` to `specific activities` to `improve concurrency`. 
  - `On a server` with **eight** `processors`, **six** `could be reserved for the DBMS` to `maximize` available `processing resources` for the database. 


 Tuning PostgreSQL Database Parameters to Optimize Performance 
---------------------------------------------------------------------------------------

The default PostgreSQL configuration is not tuned for any particular workload.

Default values are set to ensure that PostgreSQL runs everywhere, with the least resources it can consume and so that it doesn’t cause any vulnerabilities.

According to their system’s workload, the PostgreSQL database can be tuned.

While `optimizing` PostgreSQL server `configuration` `improves performance`, a database developer must also be diligent when writing queries for the application. `If queries perform full table scans` where an `index could be used` or `perform heavy joins or expensive aggregate operations`, then the `system can still perform poorly` even if the database parameters are tuned. It is important to `pay attention` to performance when `writing` database `queries`.

#### PostgreSQL’s Tuneable Parameters

1)  **shared_buffer**

`PostgreSQL uses its own buffer` and `also uses kernel buffered IO`. That means data is stored in memory twice, first in PostgreSQL buffer and then kernel buffer. Unlike other databases, PostgreSQL `does not provide direct IO`. This is called `double buffering`. The `PostgreSQL buffer` is called `shared_buffer` which is the `most effective tunable parameter` for most operating systems. This parameter `sets` how much `dedicated memory` will be `used` by PostgreSQL `for cache`.

The `default value` of `shared_buffer` is `set very low `and you will `not get much benefit` from that. It’s low because `certain machines` and `operating systems` do `not support` higher values. But `in most modern machines`, you need to `increase` this `value` for optimal `performance`.

The `recommended value` is `25%` of your total machine `RAM`. You should `try` some `lower` and `higher` values because in `some cases` we `achieve` good `performance` with a `setting` over `25%`. The `configuration` really `depends on` your `machine` and the `working data set`. 

`If your working set of data` can easily `fit into` your `RAM`, then you might want to `increase` the `shared_buffer` value `to contain your entire database`, so that the `whole working set of data` can `reside in cache`. 

That said, you obviously do not want to reserve all RAM for PostgreSQL.

`In production environments`, it is observed that `a large value` for `shared_buffer` gives really good performance, though you should `always benchmark to find the right balance`.

	testdb=# SHOW shared_buffers;
```	
shared_buffers
----------------	
	128MB
	(1 row)
```
Note: Be careful as some kernels do not allow a bigger value, specifically `in Windows` there is `no use of higher value`.


2) ***wal_buffers***

`PostgreSQL writes its WAL (write ahead log) record into the buffers` and `then these buffers are flushed to disk`. 
The `default `size of the buffer, defined by wal_buffers, is `16MB`, but `if you have a lot of concurrent connections` then a `higher value` can `give better performance`.

3) ***effective_cache_size***

* `provides an estimate of the memory available for disk caching`. 
* It is just a guideline, not the exact allocated memory or cache size. 
* `It does not allocate actual memory but tells the optimizer the amount of cache available in the kernel`. 
* If the `value of this is set too low` the `query planner can decide not to use some indexes`, even if they’d be helpful. Therefore, `setting a large value is always beneficial`.

4) ***work_mem***

- This configuration is used for `complex sorting`. 

- If you have to do complex sorting then `increase` the value of `work_mem` for good results.

- In-memory sorts are much faster than sorts spilling to disk. 

- Setting a `very high value can cause a memory bottleneck` for your deployment environment because this parameter is per user sort operation.
 
- Therefore, `if you have many users` trying to execute sort operations, then the `system will allocate work_mem * total sort operations`  for all users. 

- Setting this parameter `globally `can cause `very high memory` usage. 

- So it is highly `recommended` to modify this at the `session level`.

**2MB**
```
testdb=# SET work_mem TO "2MB";
testdb=# EXPLAIN SELECT * FROM bar ORDER BY bar.b;

                                    QUERY PLAN                                     
-----------------------------------------------------------------------------------
Gather Merge  (cost=509181.84..1706542.14 rows=10000116 width=24)
   Workers Planned: 4
   ->  Sort  (cost=508181.79..514431.86 rows=2500029 width=24)
         Sort Key: b
         ->  Parallel Seq Scan on bar  (cost=0.00..88695.29 rows=2500029 width=24)
(5 rows)
```

The initial query’s sort node has an `estimated cost of 514431.86`. 

Cost is an arbitrary unit of computation. For the above query, we have a work_mem of only 2MB. 

For testing purposes, let’s increase this to 256MB and see if there is any impact on cost.

**256MB**
```
testdb=# SET work_mem TO "256MB";
testdb=# EXPLAIN SELECT * FROM bar ORDER BY bar.b;

                                    QUERY PLAN                                     
-----------------------------------------------------------------------------------
Gather Merge  (cost=355367.34..1552727.64 rows=10000116 width=24)
   Workers Planned: 4
   ->  Sort  (cost=354367.29..360617.36 rows=2500029 width=24)
         Sort Key: b
         ->  Parallel Seq Scan on bar  (cost=0.00..88695.29 rows=2500029 width=24)
```
The **query cost is reduced to 360617.36** from 514431.86 — a **30% reduction**.

5) ***maintenance_work_mem***


- maintenance_work_mem is a memory setting used for `maintenance tasks`. 
- The `default` value is `64MB`. 
- Setting a `large value helps` in tasks like 
	- **VACUUM, RESTORE, CREATE INDEX, ADD FOREIGN KEY** and **ALTER TABLE.**

setting maintenance_work_mem = 10MB

```	
postgres=# CHECKPOINT;
postgres=# SET maintenance_work_mem to '10MB';
postgres=# CREATE INDEX foo_idx ON foo (c);
CREATE INDEX
Time: 170091.371 ms (02:50.091)
```

setting maintenance_work_mem = 256MB

```
postgres=# CHECKPOINT;
postgres=# set maintenance_work_mem to '256MB';
postgres=# CREATE INDEX foo_idx ON foo (c);
CREATE INDEX
Time: 111274.903 ms (01:51.275)
```

Indexes are a common way to enhance database performance. 

An index allows the database server to find and retrieve specific rows much faster than it could do without an index. 

But indexes also add overhead to the database system as a whole, so they should be used sensibly.

6) ***synchronous_commit***

This is used to enforce that commit will wait for WAL to be written on disk before returning a success status to the client. 
This is a trade-off between performance and reliability. 
If your application is designed such that performance is more important than the reliability, then turn off synchronous_commit. 
This means that there will be a time gap between the success status and a guaranteed write to disk. 
In the case of a server crash, data might be lost even though the client received a success message on commit. 
In this case, a transaction commits very quickly because it will not wait for a WAL file to be flushed, but reliability is compromised.

7) ***checkpoint_timeout, checkpoint_completion_target***

PostgreSQL writes changes into WAL. The checkpoint process flushes the data into the data files. This activity is done when CHECKPOINT occurs. This is an expensive operation and can cause a huge amount of IO. This whole process involves expensive disk read/write operations. Users can always issue CHECKPOINT whenever it seems necessary or automate the system by PostgreSQL’s parameters checkpoint_timeout and checkpoint_completion_target.

The checkpoint_timeout parameter is used to set time between WAL checkpoints. Setting this too low decreases crash recovery time, as more data is written to disk, but it hurts performance too since every checkpoint ends up consuming valuable system resources. The checkpoint_completion_target is the fraction of time between checkpoints for checkpoint completion. A high frequency of checkpoints can impact performance. For smooth checkpointing, checkpoint_timeout must be a low value. Otherwise the OS will accumulate all the dirty pages until the ratio is met and then go for a big flush.

The standard PostgreSQL tool for performance tests is pgbench, while for MySQL it’s SysBench. SysBench supports multiple database drivers and scriptable tests in the Lua programming language, so we decided to use this tool for both databases