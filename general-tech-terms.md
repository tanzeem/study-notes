
### Federation

A federation is a group of computing or network providers agreeing upon standards of operation in a collective fashion. 

The term may be used when describing the inter-operation of two distinct, formally disconnected, telecommunications networks that may have different internal structures

In a telecommunication interconnection, the internal modus operandi of the different systems is irrelevant to the existence of a federation. 

Joining two distinct networks: 
-----------------------------

- Matrix and Telegram is interoperable with brifge facility
- Yahoo! and Microsoft announced that Yahoo! Messenger and MSN Messenger would be interoperable

Collective authority:
---------------------

   - The MIT X Consortium was founded in 1988 to prevent fragmentation in development of the X Window System.
   - OpenID, a form of federated identity.

### ActivityPub

ActivityPub is an open, decentralized social networking protocol based on Pump.io's ActivityPump protocol.

It provides a client/server API for creating, updating and deleting content, as well as a federated server-to-server API for delivering notifications and content. 

ActivityPub is a standard for the Internet in the Social Web Networking Group of the World Wide Web Consortium (W3C). At an earlier stage, the name of the protocol was "ActivityPump", but it was felt that ActivityPub better indicated the cross-publishing purpose of the protocol. It learned from the experiences with the older standard called OStatus. 

**Server to Server federation protocol**

-     `Mastodon`, a `social networking software`, implemented ActivityPub in version 1.6, released on 10 September 2017. It is intended that ActivityPub offer more security for private messages than the previous OStatus protocol does.

-    Pleroma, a social networking software implementing ActivityPub.

-    Misskey, a social networking software implementing ActivityPub.

-    `Hubzilla`, a `community CMS software platform`, implemented ActivityPub from version 2.8 (October 2017) with a plugin.

-    `Nextcloud`, a federated service for `file hosting`.

-    `PeerTub`e, a federated service for `video streaming`.

-    `Pixelfed`, a federated service for `image sharing`.

-    Friendica, a social networking software, implemented ActivityPub in its development branch.

-    Osada, a social networking software implementing ActivityPub

### Federated Identity

A federated identity in information technology is the means of linking a person's electronic identity and attributes, stored across multiple distinct identity management systems

- Federated identity is related to single sign-on (SSO), in which a user's single authentication ticket, or token, is trusted across multiple IT systems or even organizations.

- `SSO` is a subset of federated identity management, as it relates only to authentication and is understood on the level of technical interoperability and it would not be possible without some sort of federation.

Technologies used for federated identity include `SAML (Security Assertion Markup Language)`, `OAuth`, `OpenID`, `Security Tokens` (`Simple Web Tokens`, `JSON Web Token`s, and `SAML assertions`), Web Service Specifications, and `Windows Identity Foundation`.


### Matrix\.org

- Matrix is an ambitious new ecosystem for `open federated Instant Messaging and VoIP`

- Everything in Matrix happens in a room. `Rooms` are `distributed` and `do not exist on any single server`.

- Rooms can be located using convenience aliases like `#matrix:matrix.org` or `#test:localhost:8448`

- Matrix user IDs look like `@matthew:matrix.org` (although in the future you will normally refer to yourself and others using a third party identifier (3PID): email address, phone number, etc rather than manipulating Matrix user IDs)

The overall architecture is:

client <----> homeserver <=====================> homeserver <----> client
       https://somewhere.org/_matrix      https://elsewhere.net/_matrix

[Running Synapse Homeserver](https://github.com/matrix-org/synapse)


### Net Neutrality

[TBD]

### Cloud Computing

#### Opensource Tools

**Infrastructure as a Service (IAAS)**

- OpenStack
- CloudStack
- Eucalyptus
- OpenNebul
