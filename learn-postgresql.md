
PostgreSQL
==========

PostgreSQL is a fully featured object-relational database management
system.  It supports a large part of the SQL standard and is designed
to be extensible by users in many aspects.  Some of the features are:
ACID transactions, foreign keys, views, sequences, subqueries,
triggers, user-defined types and functions, outer joins, multiversion
concurrency control.  Graphical user interfaces and bindings for many
programming languages are available as well.

PostgreSQL is a powerful, open source object-relational database
system. It is fully ACID compliant, has full support for foreign
keys, joins, views, triggers, and stored procedures (in multiple
languages). It includes most SQL:2008 data types, including INTEGER,
NUMERIC, BOOLEAN, CHAR, VARCHAR, DATE, INTERVAL, and TIMESTAMP. It
also supports storage of binary large objects, including pictures,
sounds, or video. It has native programming interfaces for C/C++,
Java, .Net, Perl, Python, Ruby, Tcl, ODBC, among others, and
exceptional documentation.



Postgresql Packages in Ubuntu 16.04 LTS
=======================================


* **postgresql-9.5** : object-relational SQL database, version 9.5 server 

This package provides the database server for PostgreSQL 9.5.


* **postgresql**: object-relational SQL database (supported version)

This metapackage always depends on the currently supported PostgreSQL
database server version.

* **postgresql-common**: PostgreSQL database-cluster manager 

The postgresql-common package provides a structure under which
multiple versions of PostgreSQL may be installed and/or multiple
clusters maintained at one time.

The commands provided are `pg_conftool`, `pg_createcluster`, `pg_ctlcluster`,
`pg_dropcluster`, `pg_lsclusters`, `pg_renamecluster`, `pg_upgradecluster`,
`pg_virtualenv`.

* **postgresql-client** : front-end programs for PostgreSQL (supported version)

This metapackage always depends on the currently supported database
client package for PostgreSQL.

* **postgresql-client-9.5** : front-end programs for PostgreSQL 9.5

This package contains client and administrative programs for
PostgreSQL: these are the interactive terminal client psql and
programs for creating and removing users and databases.

This is the client package for PostgreSQL 9.5. If you install
PostgreSQL 9.5 on a standalone machine, you need the server package
postgresql-9.5, too. On a network, you can install this package on
many client machines, while the server package may be installed on
only one machine.

* **postgresql-client-common** : manager for multiple PostgreSQL client versions

The postgresql-client-common package provides a structure under which
multiple versions of PostgreSQL client programs may be installed at
the same time. It provides a wrapper which selects the right version
for the particular cluster you want to access (with a command line
option, an environment variable, /etc/postgresql-common/user_clusters,
or ~/.postgresqlrc).

* **postgresql-contrib-9.5** : additional facilities for PostgreSQL

The PostgreSQL contrib package provides several additional features
for the PostgreSQL database. This version is built to work with the
server package postgresql-9.5.  contrib often serves as a testbed for
features before they are adopted into PostgreSQL proper:

 adminpack      - File and log manipulation routines, used by pgAdmin
 btree_gist     - B-Tree indexing using GiST (Generalised Search Tree)
 chkpass        - An auto-encrypted password datatype
 cube           - Multidimensional-cube datatype (GiST indexing example)
 dblink         - Functions to return results from a remote database
 earthdistance  - Operator for computing the distance (in miles) between
                  two points on the earth's surface
 fuzzystrmatch  - Levenshtein, metaphone, and soundex fuzzy string matching
 hstore         - Store (key, value) pairs
 intagg         - Integer aggregator/enumerator
 _int           - Index support for arrays of int4, using GiST (benchmark
                  needs the libdbd-pg-perl package)
 isn            - type extensions for ISBN, ISSN, ISMN, EAN13 product numbers
 lo             - Large Object maintenance
 ltree          - Tree-like data structures
 oid2name       - Maps OIDs to table names
 pageinspect    - Inspection of database pages
 passwordcheck  - Simple password strength checker
 pg_buffercache - Real time queries on the shared buffer cache
 pg_freespacemap- Displays the contents of the free space map (FSM)
 pg_trgm        - Determine the similarity of text based on trigram matching
 pg_standby     - Create a warm stand-by server
 pgbench        - TPC-B like benchmark
 pgcrypto       - Cryptographic functions
 pgrowlocks     - A function to return row locking information
 pgstattuple    - Returns the percentage of dead tuples in a table; this
                  indicates whether a vacuum is required.
 postgresql_fdw - foreign data wrapper for PostgreSQL
 seg            - Confidence-interval datatype (GiST indexing example)
 sepgsql        - mandatory access control (MAC) based on SELinux
 spi            - PostgreSQL Server Programming Interface; 4 examples of
                  its use:
                  autoinc    - A function for implementing AUTOINCREMENT/
                               IDENTITY
                  insert_username - function for inserting user names
                  moddatetime - Update modification timestamps
                  refint     - Functions for implementing referential
                               integrity (foreign keys).  Note that this is
                               now superseded by built-in referential
                               integrity.
                  timetravel - Re-implements in user code the time travel
                               feature that was removed in 6.3.
 tablefunc      - examples of functions returning tables
 uuid-ossp      - UUID generation functions
 vacuumlo       - Remove orphaned large objects


Installing PostqreSQL in Ubuntu 16 LTS
--------------------------------------

sudo tasksel
select postgresql
click ok

su - postgres
Password: 
su: Authentication failure

psql
psql: FATAL:  no pg_hba.conf entry for host "[local]", user "tanzeem", database "tanzeem", SSL off

Fixing
Uncommented the following line from postgresql.conf at /etc/postgresql/9.5/main/postgresql.conf

listen_addresses = 'localhost'	

This installation failed and I tried the website in digitalocean.com


Initial Server Setup with Ubuntu 16.04
=======================

ssh root@your_server_ip

adduser sammy

usermod -aG sudo sammy

-----------------------------
Add Public Key Authentication
-----------------------------

Generate a Key Pair
-------------------

ssh-keygen

ssh-keygen output
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/localuser/.ssh/id_rsa):

This generates a private key, id_rsa, and a public key, id_rsa.pub, in the .ssh directory of the localuser's home directory. Remember that the private key should not be shared with anyone who should not have access to your servers!

After generating an SSH key pair, you will want to copy your public key to your new server

Option 1: Use ssh-copy-id
If your local machine has the ssh-copy-id script installed, you can use it to install your public key to any user that you have login credentials for.

Run the ssh-copy-id script by specifying the user and IP address of the server that you want to install the key on, like this:

ssh-copy-id sammy@your_server_ip

After providing your password at the prompt, your public key will be added to the remote user's .ssh/authorized_keys file. The corresponding private key can now be used to log into the server.

Option 2: Manually Install the Key
Assuming you generated an SSH key pair using the previous step, use the following command at the terminal of your local machine to print your public key (id_rsa.pub):

cat ~/.ssh/id_rsa.pub

This should print your public SSH key, which should look something like the following:

id_rsa.pub contents
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBGTO0tsVejssuaYR5R3Y/i73SppJAhme1dH7W2c47d4gOqB4izP0+fRLfvbz/tnXFz4iOP/H6eCV05hqUhF+KYRxt9Y8tVMrpDZR2l75o6+xSbUOMu6xN+uVF0T9XzKcxmzTmnV7Na5up3QM3DoSRYX/EP3utr2+zAqpJIfKPLdA74w7g56oYWI9blpnpzxkEd3edVJOivUkpZ4JoenWManvIaSdMTJXMy3MtlQhva+j9CgguyVbUkdzK9KKEuah+pFZvaugtebsU+bllPTB0nlXGIJk98Ie9ZtxuY3nCKneB+KjKiXrAvXUPCI9mWkYS/1rggpFmu3HbXBnWSUdf localuser@machine.local

Select the public key, and copy it to your clipboard.

To enable the use of SSH key to authenticate as the new remote user, you must add the public key to a special file in the user's home directory.

On the server, as the root user, enter the following command to temporarily switch to the new user (substitute your own user name):

Now you will be in your new user's home directory.

su - sammy

Create a new directory called .ssh and restrict its permissions with the following commands:

mkdir ~/.ssh
chmod 700 ~/.ssh

Now open a file in .ssh called authorized_keys with a text editor. We will use nano to edit the file:

nano ~/.ssh/authorized_keys

Now insert your public key (which should be in your clipboard) by pasting it into the editor.

Hit CTRL-x to exit the file, then y to save the changes that you made, then ENTER to confirm the file name.

Now restrict the permissions of the authorized_keys file with this command:

chmod 600 ~/.ssh/authorized_keys

Type this command once to return to the root user:

exit
Now your public key is installed, and you can use SSH keys to log in as your user.

Step Five — Disable Password Authentication (Recommended)
Now that your new user can use SSH keys to log in, you can increase your server's security by disabling password-only authentication. Doing so will restrict SSH access to your server to public key authentication only. That is, the only way to log in to your server (aside from the console) is to possess the private key that pairs with the public key that was installed.

Note: Only disable password authentication if you installed a public key to your user as recommended in the previous section, step four. Otherwise, you will lock yourself out of your server!

To disable password authentication on your server, follow these steps.

As root or your new sudo user, open the SSH daemon configuration:

sudo nano /etc/ssh/sshd_config

Find the line that specifies PasswordAuthentication, uncomment it by deleting the preceding #, then change its value to "no". It should look like this after you have made the change:

sshd_config — Disable password authentication
PasswordAuthentication no

Here are two other settings that are important for key-only authentication and are set by default. If you haven't modified this file before, you do not need to change these settings:

sshd_config — Important defaults
PubkeyAuthentication yes
ChallengeResponseAuthentication no
When you are finished making your changes, save and close the file using the method we went over earlier (CTRL-X, then Y, then ENTER).

Type this to reload the SSH daemon:

sudo systemctl reload sshd
Password authentication is now disabled. Your server is now only accessible with SSH key authentication.

Step Six — Test Log In
Now, before you log out of the server, you should test your new configuration. Do not disconnect until you confirm that you can successfully log in via SSH.

In a new terminal on your local machine, log in to your server using the new account that we created. To do so, use this command (substitute your username and server IP address):

ssh sammy@your_server_ip

If you added public key authentication to your user, as described in steps four and five, your private key will be used as authentication. Otherwise, you will be prompted for your user's password.

Note about key authentication: If you created your key pair with a passphrase, you will be prompted to enter the passphrase for your key. Otherwise, if your key pair is passphrase-less, you should be logged in to your server without a password.

Once authentication is provided to the server, you will be logged in as your new user.

Remember, if you need to run a command with root privileges, type "sudo" before it like this:

sudo command_to_run

Step Seven — Set Up a Basic Firewall

Ubuntu 16.04 servers can use the UFW firewall to make sure only connections to certain services are allowed. We can set up a basic firewall very easily using this application.

Different applications can register their profiles with UFW upon installation. These profiles allow UFW to manage these applications by name. OpenSSH, the service allowing us to connect to our server now, has a profile registered with UFW.

You can see this by typing:

sudo ufw app list

Output
Available applications:
  OpenSSH

We need to make sure that the firewall allows SSH connections so that we can log back in next time. We can allow these connections by typing:

sudo ufw allow OpenSSH

Afterwards, we can enable the firewall by typing:

sudo ufw enable

Type "y" and press ENTER to proceed. You can see that SSH connections are still allowed by typing:

sudo ufw status

Output
Status: active

To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere
OpenSSH (v6)               ALLOW       Anywhere (v6)

[Ref](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04)


Securing PostgreSQL against automated attack
============================

One Ubuntu 16.04 PostgreSQL Database Host:

sudo apt-get update
sudo apt-get install postgresql postgresql-contrib

***This section is incomplete***


Postgresql concepts
============

## Indexes

Suppose we have a table similar to this:
```
CREATE TABLE test1 (
id integer,
content varchar
);
```
and the application issues many queries of the form:

	SELECT content FROM test1 WHERE id = constant ;
	
With no advance preparation, the system would have to scan the entire test1 table, row by row, to
find all matching entries. If there are many rows in test1 and only a few rows (perhaps zero or one)
that would be returned by such a query, this is clearly an inefficient method. But if the system has
been instructed to maintain an index on the id column, it can use a more efficient method for locating
matching rows. For instance, it might only have to walk a few levels deep into a search tree

The following command can be used to create an index on the id column, as discussed:

	CREATE INDEX test1_id_index ON test1 (id);
	
Once an index is created, no further intervention is required: the system will update the index when the
table is modified, and it will use the index in queries when it thinks doing so would be more efficient
than a sequential table scan. 

But you might have to run the **ANALYZE** command regularly to update
statistics to allow the **query planner** to make educated decisions.	
	
Indexes can also benefit UPDATE and DELETE commands with search conditions. Indexes can more-
over be used in join searches. Thus, an index defined on a column that is part of a join condition can
also significantly speed up queries with joins.

PostgreSQL provides several index types:

 - B-tree, 
 - Hash, 
 - GiST, 
 - SP-GiST, 
 - GIN and 
 - BRIN. 
 
 Each index type uses a different algorithm that is best suited to different types of queries. 
 
` By default`, the `CREATE INDEX` command creates `B-tree` indexes, which fit the `most common situations`.	
	
	
### Database Cluster

![cluster](/home/tanzeem/Documents/learn/Postgresql/800px-PostgreSQL_cluster_1.svg.png  "Database Cluster")

**Server (or Node)**
A server is some (real or virtual) hardware where PostgreSQL is installed. Please don't exchange it with the term instance.

**Cluster of Nodes**
A set of nodes, which interchange information via replication.

**Installation**
After you have downloaded and installed PostgreSQL, you have a set of programs, scripts, configuration- and other files on a server. This set is called the 'Installation'. It includes all instance programs as well as some client programs like psql.

**Server Database**
The term server database is often used in the context of client/server connections to refer to an instance or a single database.

**Cluster (or 'Database Cluster')**
A cluster is a storage area (directory, subdirectories and files) in the file system, where a collection of databases plus meta-information resides. Within the database cluster there are also the definitions of global objects like users and their rights. They are known across the entire database cluster. (Access rights for an user may be limited to individual objects like a certain table or a certain schema, thus it is defined that the user did not have this access rights to the other objects of the cluster.) `Within a database cluster there are at least three databases: 'template0', 'template1', 'postgres' and possibly more`.

- 'template0': A template database, which may be used by the command CREATE DATABASE 
  (template0 should never be modified)
- 'template1': A template database, which may be used by the command CREATE DATABASE (template1 may be modified by DBA)
- 'postgres': An empty database, mainly for maintenance purposes

`Most PostgreSQL installations use only one database cluster. Its name is 'main'.` 

But you can `create more clusters` on the same PostgreSQL installation, see tools `initdb` further down.

**Instance (or 'Database Server Instance' or 'Database Server' or 'Backend')**

`An instance is a group of processes` (on a UNIX server) respectively one service (on a Windows server) `plus shared memory`, which `controls and manages exactly one cluster`.

Using IP terminology one can say that one instance occupies one IP/port combination, eg. the combination http://localhost:5432. 

It is possible that on a different port of the same server another instance is running. 

The `processes` (in a UNIX server), `which build an instance`, are called: 
- `postmaster` (creates one 'postgres'-process per client-connection), 
- `logger`, 
- `checkpointer`, 
- `background writer`, 
- `WAL writer`, 
- `autovacuum launcher`, 
- `archiver`, 
- `stats collector`. 

If you have many clusters on your server, you can run many instances at the same machine - one per cluster.

Hint: Other publications sometimes use the term server to refer to an instance. As the term server is widely used to refere to real or virtual hardware, we do not use server as a synonym for instance.

**Database**

A database is a storage area in the file system, where a collection of objects is stored in files. The objects consist of data, metadata (table definitions, data types, constraints, views, ...) and other data like indices. Those objects are stored in the default database 'postgres' or in a newly created database.

The storage area for one database is organized as one subdirectory tree within the storage area of the database cluster. Thus a database cluster may contain multiple databases.

In a newly created database cluster (see below: initdb) there is an empty database with the name 'postgres'. In most cases this database keeps empty and application data is stored in separate databases like 'finance' or 'engineering'. Nevertheless 'postgres' shall not be droped because some tools try to store temporary data within this database.

**Schema**

 - A schema is `a namespace within a database`: 
 
 - `Contains named objects (tables, data types, functions, and operators)` whose names can duplicate those of other objects existing in other schemas of this database. 
 
 - `Every database contains the default schema 'public'` and may contain more schemas. 
 
 - `All objects of one schema must reside within the same database`. 
 
- `Objects of different schemas within the same database may have the same name`. 
 
- There is another special schema in each database. 
`The schema 'pg_catalog' contains all system tables, built-in data types, functions, and operators`. See also 'Search Path' below.

**Search Path (or 'Schema Search Path')**

A Search Path is a `list of schema names`. If applications use unqualified object names (e.g.: 'employee_table' for a table name), the search path is used to locate this object in the given sequence of schemas. The schema 'pg_catalog' is always the first part of the search path although it is not explicitly listed in the search path. This behaviour ensures that PostgreSQL finds the system objects.

**initdb (OS command)**

Despite of its name the utility `initdb creates a new cluster`, which `contains the 3 databases 'template0', 'template1' and 'postgres'`.

**createdb (OS command)**
The utility createdb creates a new database within the actual cluster.

CREATE DATABASE (SQL command)
The SQL command CREATE DATABASE creates a new database within the actual cluster.

**Directory Structure**
A cluster and its databases consists of files, which hold data, actual status information, modification information and a lot more. Those files are organized in a fixed way under one directory node. 

![Directory Structure](/home/tanzeem/Documents/learn/Postgresql/800px-PostgreSQL_cluster_2.svg.png  "Directory Structure")

Consistent Writes
Shared Buffers
Shared bufferes are RAM pages, which mirror pages of data files on disc. They exist due to performance reasons. The term shared results from the fact that a lot of processes read and write to that area.

'Dirty' Page
Pages in the shared bufferes mirror pages of data files on disc. When clients request changes of data, those pages get changed without - provisionally - a change of the according pages on disc. Until the background writer writes those modified pages to disc, they are called 'dirty' pages.

Checkpoint
A checkpoint is a special point in time where it is guarantied that the database files are in a consistent state. At checkpoint time all change records are flushed to the WAL file, all dirty data pages (in shared buffers) are flushed to disc, and at last a special checkpoint record is written to the WAL file. 
The instance's checkpointer process automatically triggers checkpoints on a regular basis. Additionally they can be forced by issuing the command CHECKPOINT in a client program. For the database system it takes a lot of time to perform a checkpoint - because of the physical writes to disc.

WAL File
WAL files contain the changes which are applied to the data by modifiing commands like INSERT, UPDATE, DELETE or CREATE TABLE ... . This is redundant information as it is also recorded in the data files (for better performance at a later time). According to the configuration of the instance there may be more information within WAL files. WAL files reside in the pg_wal directory (which was named pg_xlog before version 10), has a binary format, and have a fix size of 16MB. When they are no longer needed, they get recycled by renaming and reuseing their already allocated space.
A single information unit within a WAL file is called a log record.
Hint: In the PostgreSQL documentation and in related documents there are a lot of other, similar terms which refer to what we denote WAL file in our Wikibook: segment, WAL segment, logfile (don't mix it with the term logfile, see below), WAL log file, ... .

Logfile
The instance logs and reports warning and error messages about special situations in readable text files. This logfiles reside at any place in the directory structure of the server and are not part of the cluster.
Hint: The term 'logfile' does not relate to the other terms of this subchapter. He is mentioned here because the term sometimes is used as a synonym for what we call WAL file - see above.

Log Record
A log record is a single information unit within a WAL file.

Segment
The term segment is sometimes used as a synonym for WAL file.

MVCC
Multiversion Concurrency Control (MVCC) is a common database technique to accomplish two goals: First, it allows the management of parallel running transactions on a logical level and second, it ensures high performance for concurrent read and write actions. It is implemented as follows: Whenever values of an existing row changes, PostgreSQL writes a new version of this row to the database without deleting to old one. In such situations the database contains multiple versions of the row. In addition to their regular data the rows contain transaction IDs which allows to decide, which other transactions will see the new or the old row. Hence other transactions sees only those values (of other transactions), which are commited.
Outdated old rows are deleted at a later time by the utility vacuumdb respectively the SQL command vacuum.

Backup and Recovery
The term cold as an addition to the backup method name indicates that with this method the instance must be stopped to create a useful backup. In contrast, the addition 'hot' denotes methods where the instance MUST run (and hence changes to the data may occur during backup actions).

(Cold) Backup (file system tools)
A cold backup is a consistent copy of all files of the cluster with OS tools like cp or tar. During the creation of a cold backup the instance must not run - otherwise the backup is useless. Hence you need a period of time in which no application use any database of the cluster - a continuous 7×24 operation mode is not possible. And secondly: the cold backup works only on the cluster level, not on any finer granularity like database or table.
Hint: A cold backup is sometimes called an "offline backup".

(Hot) Logical Backup (pg_dump utility)
A logical backup is a consistent copy of the data within a database or some of its parts. It is created with the utility pg_dump. Although pg_dump may run in parallel to applications (the instance must be up), it creates a consistent snapshot as of the time of its start.
pg_dump supports two output formats. The first one is a text format containing SQL commands like CREATE and INSERT. Files created in this format may be used by psql to restore the backed-up data. The second format is a binary format and is called the 'archive format'. Files with this format can be used to restore its data with the tool pg_restore.
As mentioned, pg_dump works at the database level or smaller parts of databases like tables. If you want to refer to the cluster level, you must use pg_dumpall. Please notice, that important objects like users/roles and their rights are always defined at cluster level.
Hint: A logical backup is one form of an "online backup".

(Hot) Physical Backup or 'Base Backup'
A physical backup is a possibly inconsistent copy of the files of a cluster, created with an operating system utility like cp or tar. At first glance such a backup seems to be useless. To understand its purpose, you must know PostgreSQL's recover-from-crash strategy.
At all times and independent from any backup/recovery action, PostgreSQL maintains WAL files - primarily for crash-safety purposes. WAL files contain log records, which reflect all changes made to the data. In the case of a system crash those log records are used to recover the cluster to a consistent state. The recover process searches the timestamp of the last checkpoint and replays all subsequent log records in chronological order against this version of the cluster. Through that actions the cluster gets recovered to a consistent state and will contain all changes up to the last COMMIT.
The existence of a physical backup plus WAL files in combination with this recovery-from-crash technique can be used for backup/recovery purposes. To implement this, you need a physical backup (which may reflect an inconsistent state of the cluster) and which acts as the starting point. Additionally you need all WAL files since the point in time when you have created this backup. The recover process uses the described recovery-from-crash technique and replays all log records in the WAL files against the backup. In the exact same way as before, the cluster comes to a consistent state and contains all changes up to the last COMMIT.
Please keep in mind, that physical backups work only on cluster level, not on any finer granularity like database or table.
Hint: A physical backup is one form of an "online backup".

PITR: Point in Time Recovery
If the previously mentioned physical backup is used during recovery, the recovery process is not forced to run up to the latest available timestamp. Via a parameter you can stop him at a time in the past. This leads to a state of the cluster at this moment. Using this technique you can restore your cluster to a time, which is between the time of creating the physical backup and the end of the last WAL file.

Standalone (Hot) Backup
The standalone backup is a special variant of the physical backup. It offers online backup (the instance keeps running) but it lacks the possibility of PITR. The recovery process recovers always up to the end of the standalone backup process. WAL files, which arise after this point in time, cannot be applied to this kind of backup. Details are described here.

Archiving
Archiving is the process of copying WAL files to a failsafe location. When you plan to use PITR you must ensure that the sequence of WAL files is saved for a longer period. To support the process of copying WAL files at the right moment (when they are completely filled and a switch to the next WAL file has taken place), PostgreSQL runs the archiving process which is part of the instance. This process copies WAL files to a configurable destination.

Recovering
Recovering is the process of playing WAL files against a physical backup. One of the involved steps is the copy of the WAL files from the failsafe archive location to its original location in '/pg_xlog'. The aim of recovery is bringing the cluster into a consistent state at a defined timestamp.

Archive Recovery Mode
When recovering takes place, the instance is in archive recovery mode.

Restartpoint
A restartpoint is an action similar to a checkpoint. Restartpoints are only performed when the instance is in archive recovery mode or in standby mode.

Timeline
After a successful recovery PostgreSQL transfers the cluster into a new timeline to avoid problems, which may occur when PITR is reset and WAL files reapplied (e.g.: to a different timestamp). Timeline names are sequential numbers: 1, 2, 3, ... .

Replication
Replication is a technique to send data, which was written within a master server, to one or more standby servers or even another master server.

Master Server
The master server is an instance on a server which sends data to other instances - additionally to its local processing of data.

Standby Server
The standby server is an instance on a server which receives information from a master server about changes of its data.

Warm Standby Server
A warm standby server is a running instance, which is in standby_mode (recovery.conf file). It continuously reads and processes incoming WAL files (in the case of log-shipping) or log records (in the case of streaming replication). It does not accept client connections.

Hot Standby Server
A hot standby server is a warm standby server with the additional flag hot_standby in postgres.conf. It accepts client connections and read-only queries.

Synchronous Replication
Replication is called synchronous, when the standby server processes the received data immediately, sends a confirmation record to the master server and the master server delays its COMMIT action until he has received the confirmation of the standby server.

Asynchronous Replication
Replication is called asynchronous, when the master server sends data to the standby server and does not expect any feedback about this action.

Streaming Replication
The term is used when log entries are transfered from master server to standby server over a TCP connection - in addition to their transfer to the local WAL file. Streaming replication is asynchronous by default but can also be synchronous.

Log-Shipping Replication
Log shipping is the process of transfering WAL files from a master server to a standby server. Log shipping is an asynchronous operation.

### PostgreSQL/Architecture

https://en.wikibooks.org/wiki/PostgreSQL/Architecture

`The daily work as a PostgreSQL DBA is based on the knowledge of PostgreSQL's architecture:`

- strategy, 
- processes, 
- buffers, 
- files, 
- configration, 
- backup and recovery, 
- replication, and a lot more. 

The page on hand describes the most basic concepts.

PostgreSQL is a relational database management system with a client-server architecture. At the server side the PostgreSQL's processes and shared memory work together and build an instance, which handles the accesses to the data. Client programs connect oneself to the instance and request read and write operations.

**The Instance**

The `instance always consists of multiple processes`. PostgreSQL does not use a multi-threaded model:

- postmaster process
- Multiple postgres processes, one for each connection
- WAL writer process
- background writer process
- checkpointer process
- autovacuum launcher process (optional)
- logger process (optional)
- archiver process (optional)
- stats collector process (optional)
- WAL sender process (if Streaming Replication is active)
- WAL receiver process (if Streaming Replication is active)
- background worker processes (if a query gets parallelised, which is available since 9.6)

![Postgresql Architecture](/home/tanzeem/Documents/learn/Postgresql/PostgreSQL_processes_1.png  "Postgresql Architecture")

https://en.wikipedia.org/wiki/PostgreSQL
